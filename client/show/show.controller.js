(function () {
    angular
        .module("DepartmentApp", [])
        .controller("ShowCtrl", ShowCtrl);

    ShowCtrl.$inject = ["$http"];

    function ShowCtrl($http) {
        var vm = this;

        vm.searchForm = {
            dept_no: "",
            dept_name: "",
            emp_no: ""
        };

        vm.editorEnabled = false;
        vm.editableDeptName = "";

        vm.enableEditor = function () {
            vm.editorEnabled = true;
            vm.editableDeptName = vm.result.dept_name;
        };

        vm.disableEditor = function () {
            vm.editorEnabled = false;
        };


        vm.departments = [];

        vm.search = function () {

            $http
                .get("/api/department_number/" + vm.searchForm.dept_no)
                .then(function (response) {
                    console.log("Search data" + JSON.stringify(response.data));
                    vm.result = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });


        };

        vm.save = function () {
            vm.result.dept_name = vm.editableDeptName;
            $http.put("/api/department_name",
                {
                    dept_no: vm.result.dept_no,
                    dept_name: vm.result.dept_name
                })
                .then(function (response) {
                    console.log(response)
                })
                .catch(function (error) {
                    console.log(error);
                })
            vm.disableEditor();
        };

        vm.deleteManager = function () {
            $http.delete("/api/department_manager/"+vm.result.manager.emp_no)
                .then(function (response) {
                    console.log(response)
                    vm.search();
                })
                .catch(function(error){
                    console.log(error);
                })

        }


    };
})();
