(function () {
    angular
        .module("DepartmentApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http"];

    function SearchCtrl($http) {
        var vm = this;

        vm.deptName = '';
        vm.result = null;
        vm.showManager = false;

        // Exposed functions
        vm.search = search;
        vm.searchForManager = searchForManager;

        // Initial calls
        init();
        function init() {
            $http
                .get("/api/departments")
                .then(function (response) {
                    console.log(response.data);
                    vm.departments = response.data;
                })
                .catch(function () {
                    // handle error here
                });
        }

        // Function definition
        function search() {
            vm.showManager = false;
            $http
                .get("/api/departments",
                    {params: {'dept_name': vm.deptName}})
                .then(function (response) {
                    vm.departments = response.data;
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
        }

        vm.getManager = function (department) {
            //Get last manager
            if (department.managers) {
                return department.managers[department.managers.length - 1]

            } else {
                return {};
            }
        }
        function searchForManager() {
            vm.showManager = true;
            $http.get("/api/departmentManager/",
                {params: {'dept_name': vm.deptName}})
                .then(function (response) {
                    vm.departments = response.data;
                    console.info("success: " + JSON.stringify(vm.departments));
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });

        }

    }

})();